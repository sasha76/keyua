<?php


namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeeCommand extends Command
{
    /*вынести в отдельный файл*/
    protected $employeeSkills = [
        'programmer' => [
            'write_code',
            'test_code',
            'communication_with_manager'
        ],
        'designer' => [
            'paint',
            'communication_with_manager'
        ],
        'testing' => [
            'test_code',
            'communication_with_manager',
            'tasks'
        ],
        'manager' => [
            'tasks'
        ]
    ];

    public function __construct(string $name = 'employee:can')
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this
            ->setDescription('test.')
            ->setHelp('test...')
            ->addArgument(
                'employee',
                InputArgument::OPTIONAL,//InputArgument::OPTIONAL
                'employee'
            )
            ->addArgument(
                'skill',
                InputArgument::OPTIONAL,//InputArgument::OPTIONAL
                'skill'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (array_key_exists($input->getArgument('employee'), $this->employeeSkills)) {
            $employee = $this->employeeSkills[(string)$input->getArgument('employee')];
            $skill = in_array((string)$input->getArgument('skill'), $employee) ? 'true' : 'false';
        } else {
            $skill = 'no employee';
        }

        $output->writeln([
            $skill
        ]);
    }
}