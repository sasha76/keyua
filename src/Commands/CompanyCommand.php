<?php


namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompanyCommand extends Command
{
    /*вынести в отдельный файл*/
    protected $employeeSkills = [
        'programmer' => [
            'write_code',
            'test_code',
            'communication_with_manager'
        ],
        'designer' => [
            'paint',
            'communication_with_manager'
        ],
        'testing' => [
            'test_code',
            'communication_with_manager',
            'tasks'
        ],
        'manager' => [
            'tasks'
        ]
    ];

    public function __construct(string $name = 'company:employee')
    {
        parent::__construct($name);
    }

    public function configure()
    {
        $this
            ->setDescription('test.')
            ->setHelp('test...')
            ->addArgument(
                'employee',
                InputArgument::OPTIONAL,//InputArgument::OPTIONAL
                'help text'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (array_key_exists($input->getArgument('employee'), $this->employeeSkills)) {
            $employee = $this->employeeSkills[(string)$input->getArgument('employee')];
            $employee = implode(' - ', $employee);
        } else {
            $employee = 'no employee';
        }

        $output->writeln([
            $employee
        ]);
    }
}